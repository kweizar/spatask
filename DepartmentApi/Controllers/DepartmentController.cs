﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DepartmentApi.Models;

namespace DepartmentApi.Controllers
{
    public class DepartmentController : ApiController
    {
        static Department[] departments =  {
            new Department { Id = 0, Title = "Sales" },
            new Department { Id = 1, Title = "Management" },
            new Department { Id = 2, Title = "Engineering" },
            new Department { Id = 3, Title = "Helpdesk" }
        };
        // GET: api/Department
        public IEnumerable<Department> Get()
        {
            return departments;
        }

        // GET: api/Department/5
        public Department Get(int id)
        {
            return departments.FirstOrDefault(e => e.Id == id);
        }

        // POST: api/Department
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Department/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Department/5
        public void Delete(int id)
        {
        }
    }
}
