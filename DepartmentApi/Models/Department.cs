﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DepartmentApi.Models
{
    public class Department
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}