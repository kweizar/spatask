﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using UserApi.Models;
namespace UserApi.Controllers
{
    public class UserController : ApiController
    {
        // не стал писать репозиторий и просто задал коллекцию через статическое свойство
        static List<User> users = new List<Models.User> {
            new User { Id=0, UserName="Alex", DepartmentId=0 },
            new User { Id=1, UserName="Bob", DepartmentId=1 },
            new User { Id=2, UserName="Joseph", DepartmentId=0 },
            new User { Id=3, UserName="Mark", DepartmentId=2 },
            new User { Id=4, UserName="Nicola", DepartmentId=3 },
            new User { Id=5, UserName="Philip", DepartmentId=0 },
            new User { Id=6, UserName="Zach", DepartmentId=1 }
        };
        // GET: api/User
        public IEnumerable<User> Get()
        {
            return users.OrderBy(e => e.Id);
        }

        // GET: api/User/5
        public User Get(int id)
        {
            return users.FirstOrDefault(e => e.Id == id);
        }

        // POST: api/User
        public void Post(User user)
        {
            user.Id = users.Max(e => e.Id) + 1;
            users.Add(user);
            Debug.WriteLine("Добавлен юзер");
        }

        // PUT: api/User/5
        public void Put(User user)
        {
            users.Remove(users.FirstOrDefault(e => e.Id == user.Id));
            users.Add(user);
        }

        // DELETE: api/User/5
        public void Delete(int id)
        {
            users.Remove(users.FirstOrDefault(e => e.Id == id));
        }
    }
}
